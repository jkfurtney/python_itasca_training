#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/
#+TITLE: Basic Python
#+OPTIONS: toc:nil num:nil reveal-theme:white

* Overview

- Basic types
- Containers
- Files and Functions

* Numbers

#+BEGIN_SRC python :session :exports both :results pp
1.3 + 4.3e-2
#+END_SRC

#+REVEAL: split

#+BEGIN_SRC python :session :exports both :results pp
a = 1.3
b = 4.3e-2
a + b
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
12**2
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
import math

math.pi * math.cos(1.77) + math.sqrt(2)
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
from math import pi, cos, sqrt

pi * cos(1.77) + sqrt(2)
#+END_SRC

** Importing Modules

#+REVEAL: split
#+BEGIN_SRC python
import itasca
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python
import itasca as it
#+END_SRC


#+REVEAL: split
#+BEGIN_SRC python
from itasca import ballarray as ba
#+END_SRC


#+REVEAL: split
Avoid this:
#+BEGIN_SRC python
from math import *
#+END_SRC
This can create confusing namespace conflicts.

* Strings

#+BEGIN_SRC python :session :exports code :results pp
joke      = "It's hard to explain puns to kleptomaniacs"
punchline = 'because they always take things literally.'

poem = """
The Ogre does what ogres can,
Deeds quite impossible for Man,
But one prize is beyond his reach:
The Ogre cannot master Speech.

About a subjugated plain,
Among its desperate and slain,
The Ogre stalks with hands on hips,
While drivel gushes from his lips.
"""
#+END_SRC
#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
"Spam " + "eggs"
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
"Spam " * 10
#+END_SRC


#+REVEAL: split
Converting objects to strings
#+BEGIN_SRC python :session :exports both :results pp
str(42) == "42"
#+END_SRC

and strings to objects
#+BEGIN_SRC python :session :exports both :results pp
int("42") == 42
#+END_SRC

#+BEGIN_SRC python :session :exports both :results pp
float("1.23e5") == 1.23e5
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
"one, two, three".split(",")
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
", ".join(["one", "two", "three"])
#+END_SRC

#+REVEAL: split
#+begin_SRC python :exports both :results output pp
laws = """ First Law of Thermodynamics: You can't win.
           Second Law of Thermodynamics: You can't break even.
           Third Law of Thermodynamics: You can't stop playing."""

for law in laws.splitlines():
    print law.split(":")[1]
#+END_SRC

** String formatting

#+BEGIN_SRC python :session :exports both :results pp
"my name is Jason"
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results pp
"my name is {}".format("Jason")
#+END_SRC


#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
template = "my name is {}"
print template.format("Jason")
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
ratio = 1.43543e-5
zone_id = 302
stress = 6.877342e6
template = "The solve ratio is {:.1e} and the stress of zone {} is {:.1e} Pa."
print template.format(ratio, zone_id, stress)
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python
import itasca as it

size_x, size_y, size_z = 10, 20, 10

it.command("""
model new
zone create brick size {x} {y} {z}
zone cmodel assign elastic
zone property density 2950 young 12e9 poisson 0.25
""".format(x=size_x, y=size_y, z=size_z))
#+END_SRC

* Containers
- Lists
- Tuples
- Dictionaries
- (Sets)

** Lists

#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4]
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
print data
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4]
len(data)
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4]
data.append(103)
data
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]

for d in data:
    print "The value of d is", d

#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]

for i, d in enumerate(data):
    print "The {} value of d is {}".format(i, d)

#+END_SRC

#+REVEAL: split
Indexing starts at zero
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]
data[0]
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]
data[-1]
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]
data[1:3]
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]
data[2] = 99.0
data
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]
data.remove(1)
data
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
data = [1, 2, 3, 4, 103]
del data[-1]
data
#+END_SRC

** Tuples
- Tuples are like lists but cannot be changed after creation.
- Many FLAC3D and PFC functions return tuples.

#+BEGIN_SRC python :session :exports both :results output pp
data = (1, 3, "Itasca")
#+END_SRC


** Dictionary

Key, value pairs of any type
#+BEGIN_SRC python :session :exports both :results output pp
properties = {"young" : 1e9, "poisson": 0.221, "density" : 2500.0}
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
properties['young']
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
properties['kn'] = 1e6
properties
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
"density" in properties
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
"cohesion" in properties
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
for key, value in properties.iteritems():
    print "Property: {} has value {:.02e}".format(key, value)
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
del(properties['density'])
properties
#+END_SRC

* Files and Functions

#+BEGIN_SRC python :session :exports both :results output pp
def my_function(a, b):
    "Add two numbers"
    return a + b

my_function(1.0, 5)
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
def test_number(n):
    if n > 1000:
       print "Very Big"
    elif n > 100:
       print "Big"
    else:
       print "Not Big"

test_number(123)
#+END_SRC


#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
outfile = open("data.txt", "w")
print >> outfile, "writing data to files is easy"
for i in range(6):
    print >> outfile, i, 4.352 * i**2

outfile.close()
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
infile = open("data.txt", "r")
for line in infile.readlines():
    print line,
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
with open("data.txt", "r") as infile:
    for line in infile.readlines():
        print line,
#+END_SRC

* =numpy= arrays
#+BEGIN_SRC python :session :exports both :results output pp
import numpy as np
np.loadtxt("data.txt", skiprows=1)
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
x, y = np.loadtxt("data.txt", skiprows=1).T
x
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
2*x
#+END_SRC

#+REVEAL: split
#+BEGIN_SRC python :session :exports both :results output pp
x**2 + y + np.sqrt(19.23*y)
#+END_SRC

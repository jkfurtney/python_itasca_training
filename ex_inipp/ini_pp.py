import itasca as it
it.command("python-reset-state false")
from itasca import gridpointarray as gpa
import numpy as np
from scipy.interpolate import griddata

it.command("""
model new
model largestrain off
zone create brick size 10 10 10
zone cmodel assign elastic
zone property density 2950 young 12e9 poisson 0.25
geom import 'tmp.geom' format geometry
""")

data = np.loadtxt("pp_data.txt")
pos, pore_pressure = data[:,0:3], data[:,3]

gridpoint_porepressure = griddata(pos, pore_pressure, gpa.pos())

gpa.set_porepressure(gridpoint_porepressure)


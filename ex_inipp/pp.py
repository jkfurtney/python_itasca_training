import itasca as it
it.command("python-reset-state false")
from itasca import gridpointarray as gpa
import numpy as np
from scipy.interpolate import griddata

it.command("""
model new
model largestrain off
zone create brick size 10 10 10
zone cmodel assign elastic
zone property density 2950 young 12e9 poisson 0.25
cycle 1
""")

p = gpa.pos() * 1.2 - 1 
pos_data = p + np.random.rand(*p.shape)
x,y,z = pos_data.T 
pos_pp = 10-z + np.cos(5-x) + np.cos(5-y)

def show_points(pos, data):    
    with open("tmp.geom", "w") as output:
        print("ITASCA GEOMETRY3D", file=output)
        print("NODES ", file=output)
        for i, (p,d) in enumerate(zip(pos,data)):
            print("{} {} {} {} EXTRA 1 {}".format(i+1, p[0], p[1], p[2], d), file=output)
    it.command("geom import 'tmp.geom' format geometry")

show_points(pos_data, pos_pp)

with open("pp_data.txt", "w") as output:
    for p,d in zip(pos_data, pos_pp):
        print(p[0], p[1], p[2], d, file=output)

a = griddata(pos_data, pos_pp, gpa.pos())
gpa.set_extra(1,a)

    
import numpy as np
import pylab as plt
import itasca as it
from itasca import gridpointarray as gpa
from itasca import zonearray as za
from vec import vec3

it.command("python-reset-state false")

def zone_loop():
    id_sum = 0
    for z in it.zone.list():
        id_sum += z.id()
    return id_sum

def gp_loop():
    id_sum = 0
    for g in it.gridpoint.list():
        id_sum += g.id()
    return id_sum
    
def stress_count():
    count = 0
    for z in it.zone.list():
        if abs(z.stress_min()) < 1e6:
            count += 1
    return count
    
def extra_set():
    d = vec3((1,1,3)).norm()
    for z in it.zone.list():
        z.set_extra(1,4*(z.pos().dot(d) + 12.4))

def int_loop():
    s = 0
    for i in range(1000000):
        s += 1
    return s
    
def ppv():
    for gp in it.gridpoint.list():
        gp.set_extra(1,max(gp.extra(1), gp.vel().mag()))

    
za.ids().sum()
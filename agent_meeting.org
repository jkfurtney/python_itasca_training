#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/
#+TITLE: Python and Itasca
#+EMAIL:
#+OPTIONS: toc:nil num:nil reveal_theme:white

* Jason Furtney

- jfurtney@itascacg.com
- Joined Itasca in 2006
- Software developer and consulting engineer

* Overview

- Introduction to Python
- FLAC3D Python Demo
- Resources

* What is Python?

Python is a general purpose programming language

- Created in 1991
- Open source (BSD style license)
- Most popular teaching language

#+REVEAL: split

- Well suited for scientific and numerical programming
  - Fast array operations: =numpy=

- Used by
  - Plaxis
  - ANSYS
  - AUTOCAD
  - NASA
  - NOAA
  - ESRI
  - and many others...

* Itasca Python History

- June 2015 PFC 5.0 gets embedded Python
- March 2017 FLAC3D 6.0 gets embedded Python

* Python Advantages

- Extensive library code
- Fast
- Easy to use

** 3rd Party Libraries

- Any 3rd Party Package should work...

- Python standard library: string manipulation, data structures,
  xml/JSON parsing, email, sockets, process control, networking,
  binary/text IO, unicode, etc.

- Integration with a vast ecosystem of scientific and numerical software
  - =scipy=, FFT, numerical integration, PDE solvers, graph theory,
    statistics, machine learning, optimization, computer algebra,
    control theory, data visualization, special functions, linear
    algebra, Eigen system, interpolation, signal processing, sparse
    matrices, etc.

** Speed

|              | FISH | Python | NumPy |
|--------------+------+--------+-------|
| Zone loop    |  1.0 | 2.5 x  | 34 x  |
| Gp loop      |  1.0 | 2.5 x  | 19 x  |
| Stress count |  1.0 | 1.8 x  | 2.9 x |
| Set extra    |  1.0 | 1.3 x  | 6.4 x |
| Int sum      |  1.0 | 4.8 x  | 27 x  |


* Python and Itasca

- Embedded Python 2.7 (3.6 in FLAC3D 6.1) interpreter in Itasca Codes
- Extended Python to work with FLAC3D and PFC (balls, walls, etc.)
    - In-line documentation and on-line Sphinx documentation
    - =numpy= based array interface to PFC and FLAC3D
    - 500+ intrinsics, 900+ tests, 100% documentation coverage
- Integrate with:
  - =numpy= based distribution of scientific software
  - IPython Qt interactive console
  - Qt bindings for Python (=pyside=)

* Itasca module

#+BEGIN_SRC python
import itasca as it
#+END_SRC

- A Python module exposing the PFC and FLAC3D types
- Object oriented interface

#+REVEAL: split

- Objects to represent: Balls, Clumps, Pebbles, Contacts, Walls,
  Zones, Gridpoints, Structural elements & cetera.

- Static functions in the =itasca= module

#+BEGIN_SRC python
ball = it.ball.find(123)
print ball.radius()
ball.set_radius(0.1223)
#+END_SRC

- "custom" oo interface, not the same as *the* interface or FISH
  - *pythonic* (using built-in types, Python conventions and idioms)
- Evaluate Itasca commands from Python
- Call Python functions during cycling sequence

** FLAC3D Applications
*** Peak Particle Velocity
- A measure of blast induced vibration intensity
   - defined as the maximum velocity magnitude of a point
- Track PPV of each gridpoint for each step

#+REVEAL: split
#+BEGIN_SRC python
for gp in it.gridpoint.list():
    gp.set_extra(1, max(gp.extra(1), gp.vel().mag()))
#+END_SRC

Faster NumPy version

#+BEGIN_SRC python
ppv = np.maximum(np.linalg.norm(gpa.vel(), axis=1), ppv)
#+END_SRC

#+REVEAL: split
- With a cycling call back
- Save/restore this data via extra variables
#+BEGIN_SRC python
ppv = np.zeros(it.gridpoint.count())

def store_ppv(*args):
    ppv = np.maximum(np.linalg.norm(gpa.vel(), axis=1), ppv)

it.set_callback("store_ppv", 0.1)
#+END_SRC

*** Pore pressure initialization
- We are given a random point cloud with pore pressure values
- We want to interpolate these onto a FLAC3D grid

#+BEGIN_SRC none
# x, y, z, pore pressure
-0.671886249211 -0.0860854702621 -0.640047518543 11.8240099491
0.882172213383 -0.722184760243 -0.238217899609 10.5247958061
-0.85511544502 1.04223589649 -0.716633071104 10.9413863163
-0.743181838374 -0.100918241486 0.627493485055 10.6090411176
0.443056607558 0.254284661304 -0.337634257426 10.2161341171
#+END_SRC

#+REVEAL: split

#+BEGIN_SRC python
import itasca as it
from itasca import gridpointarray as gpa
import numpy as np
from scipy.interpolate import griddata

data = np.loadtxt("pp_data.txt")
pos, pore_pressure = data[:,0:3], data[:,3]

gridpoint_porepressure = griddata(pos, pore_pressure, gpa.pos())

gpa.set_pp(gridpoint_porepressure)
#+END_SRC

** Resources

itascacg.com page:

https://www.itascacg.com/software/products/pfc/features/python-scripting


PFC Python Video:

  https://www.youtube.com/watch?v=iI_3ddjZRJI

FLAC3D Python Video:

  https://www.youtube.com/watch?v=Jhf2XWOu-v8

FLAC/DEM Paper:

Furtney, J.K., Purvance, M. & Emam, S. 2016. "Using the Python
programming language with Itasca software", Proceedings of the Fourth
International FLAC/DEM Symposium (Lima, Peru, March, 2016). Minneapolis, Itasca.

* Summary

- Python is fast, easy to use, and comes with lots of scientific
  programming functionality.

- Array oriented programming with NumPy

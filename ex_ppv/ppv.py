import itasca as it
it.command('python-reset-state false')
import numpy as np
np.set_printoptions(threshold=20)
from itasca import gridpointarray as gpa

ppv = None
def store_ppv(*args):
    global ppv
    if ppv is None:
        ppv = np.zeros(it.gridpoint.count())
    ppv = np.maximum(np.linalg.norm(gpa.vel(), axis=1), ppv)


it.set_callback("store_ppv", 0.1)

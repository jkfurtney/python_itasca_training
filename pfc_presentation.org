#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/latest/
#+TITLE: Python and PFC
#+EMAIL:
#+OPTIONS: toc:nil num:nil reveal_theme:white
* Python and PFC

- Python is a general purpose programming language

- Extensive scientific programming functionality

- Python is integrated into FLAC3D and PFC

** PFC Resources

Help->Help->Scripting->Python

https://www.itascacg.com/software/products/pfc/features/python-scripting

PFC Python Video:
  https://www.youtube.com/watch?v=iI_3ddjZRJI

FLAC3D Python Video:

  https://www.youtube.com/watch?v=Jhf2XWOu-v8

FLAC/DEM Paper:

Furtney, J.K., Purvance, M. & Emam, S. 2016. "Using the Python
programming language with Itasca software", Proceedings of the Fourth
International FLAC/DEM Symposium (Lima, Peru, March, 2016). Minneapolis, Itasca.



** Advantages

- Extensive (free) library code
- Fast
- Easy to use

** Speed

|              | FISH | Python | NumPy |
|--------------+------+--------+-------|
| Zone loop    |  1.0 | 2.5 x  | 34 x  |
| Gp loop      |  1.0 | 2.5 x  | 19 x  |
| Stress count |  1.0 | 1.8 x  | 2.9 x |
| Set extra    |  1.0 | 1.3 x  | 6.4 x |
| Int sum      |  1.0 | 4.8 x  | 27 x  |

(These results are for FLAC3D.)

** PFC Example
- Pipe flow example
- Apply a drag force to each particle
- Fluid velocity is a quadratic function of radial distance from pipe
  wall
- Drag force is a function of Ball/Fluid relative velocity

$\vec{f} = \frac{1}{2} C_d \rho \pi r^2 \left( \vec{u} - \vec{v}
\right) \left| \vec{u} - \vec{v} \right|$

$\vec{u} = 1.0(r_b - \frac{D_p}{2})^2$

#+REVEAL: split
Relative speed for pipe flow example

|        | component | vector | NumPy Array |
|--------+-----------+--------+-------------|
| Fish   | 1.0 x     | 1.5 x  |             |
| Python | 1.6 x     | 1.9 x  | 10 x        |
| C++    |           |        | 17 x        |
|        |           |        |             |

*** Fish component
#+BEGIN_SRC itasca-pfc5
[global rho_f = 1000.0]
[global Cd = 0.3]
[global factor = -0.5 * rho_f * Cd * math.pi]
[global pipe_r = 1.0]

def fish_force_comp()
  loop foreach local ball ball.list
    local rad = ball.radius(ball)
    local pos_y = ball.pos.y(ball)
    local pos_z = ball.pos.z(ball)
    local pos_r = math.sqrt(pos_y^2 + pos_z^2)
    local ux = ball.vel.x(ball)
    local uy = ball.vel.y(ball)
    local uz = ball.vel.z(ball)
    ux = ux - (pos_r - pipe_r)^2
    local fx = factor * rad^2 * ux * math.abs(ux)
    local fy = factor * rad^2 * uy * math.abs(uy)
    local fz = factor * rad^2 * uz * math.abs(uz)
    ball.force.app.x(ball) = fx
    ball.force.app.y(ball) = fy
    ball.force.app.z(ball) = fz
  end_loop
end
#+END_SRC

*** Fish vector
#+BEGIN_SRC itasca-pfc5
def fish_force_vec()
  loop foreach local ball ball.list
    local rad = ball.radius(ball)
    local pos_y = ball.pos.y(ball)
    local pos_z = ball.pos.z(ball)
    local pos_r = math.sqrt(pos_y^2 + pos_z^2)
    local u = ball.vel(ball)
    local v = vector((pos_r - pipe_r)^2, 0.0, 0.0)
    local rel = u-v
    local force = factor * rad^2 * rel * math.mag(rel)
    ball.force.app(ball) = force
  end_loop
end
#+END_SRC

*** Python component
#+BEGIN_SRC python
pipe_r = 1.0
rho_f = 1000.0
Cd = 0.3
factor = -0.5 * rho_f * Cd * math.pi
def py_force_comp():
    for b in it.ball.list():
        r = b.radius()
        pos_y, pos_z = b.pos_y(), b.pos_z()
        pos_r = math.sqrt(pos_y**2 + pos_z**2)
        ux,uy,uz = b.vel_x(), b.vel_y(), b.vel_z()
        ux -= (pos_r - pipe_r)**2
        fx = factor * r**2 * ux * abs(ux)
        fy = factor * r**2 * uy * abs(uy)
        fz = factor * r**2 * uz * abs(uz)
        b.set_force_app_x(fx)
        b.set_force_app_y(fy)
        b.set_force_app_z(fz)
#+END_SRC
*** Python vector
#+BEGIN_SRC python
def py_force_vec():
    for b in it.ball.list():
        r = b.radius()
        pos_y, pos_z = b.pos_y(), b.pos_z()
        pos_r = math.sqrt(pos_y**2 + pos_z**2)
        u = b.vel()
        v = vec(((pos_r - pipe_r)**2, 0.0, 0.0))
        rel = u - v
        nrel = math.sqrt(rel.dot(rel))
        force = factor * r**2 * rel * nrel
        b.set_force_app(force)
#+END_SRC

*** =numpy= array expression

- =numpy= module
  - Fast flexible n-dimensional array operations
  - 10x to 100x faster than Python loops
  - Most looping operations can be expresses as array operations
- =itasca.ballarray= module

#+BEGIN_SRC python
from itasca import ballarray as ba
def numpy_force():
    r = ba.radius()
    pos = ba.pos()
    pos[:,0] = 0.0
    pos_r = np.linalg.norm(pos,axis=1)
    u = ba.vel()
    v = np.zeros_like(u)
    v[:,0] = (pos_r-pipe_r)**2
    rel = u-v
    force = factor * r**2 * rel.T * np.linalg.norm(rel, axis=1)
    ba.set_force_app(force.T)
#+END_SRC
